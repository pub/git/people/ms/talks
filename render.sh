#!/bin/bash

PDFLATEX="pdflatex"

TALKS="2012_-_Introduction_to_the_Pakfire_Build_Service"
TALKS="${TALKS} 2014_-_A_look_into_the_past_and_future"
TALKS="${TALKS} 2015_-_IPFire_3_Networking"
TALKS="${TALKS} 2015_-_Submitting_Patches"

for talk in ${TALKS}; do
	(
		pushd "${talk}"
		[ "main.tex" -nt "main.pdf" ] || exit 0

		if ${PDFLATEX} main < /dev/null; then
			cat "main.pdf" > "../${talk}.pdf"
		fi
		popd
	)
done
