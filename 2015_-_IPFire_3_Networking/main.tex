
\documentclass[serif,mathserif,compress]{beamer}

\usepackage{beamerthemesplit}

\usetheme{default}
\useoutertheme{default}

\usepackage[british]{babel}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{color}
\usepackage{epsfig}
\usepackage{listings}
\usepackage{marvosym}
\usepackage{texnansi}
\usepackage{verbatim}
\usepackage{xspace}

% Make this a 16:9 presentation
\setlength{\paperwidth}{171 mm}
\setlength{\paperheight}{96 mm}
\setlength{\textwidth}{151 mm}
\setlength{\textheight}{86 mm}

\usepackage[default,osfigures,scale=0.95]{opensans}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}

% Set sans-serif font.
%\renewcommand\sfdefault{phv}
%\renewcommand\familydefault{\sfdefault}

% Define some colours.
\definecolor{myred}{rgb}{0.53,0.01,0}
\definecolor{mygrey}{rgb}{0.3,0.3,0.3}

% Make a nice gradient as background.
%\setbeamertemplate{background canvas}[vertical shading]
%[bottom=black, middle=myred, top=myred]
\setbeamercolor{background canvas}{bg=white, fg=mygrey}
\setbeamercolor{frametitle}{fg=mygrey,bg=mygrey!20}
\setbeamercolor{section in head/foot}{bg=myred}
\setbeamercolor{subsection in head/foot}{fg=white}
\setbeamercolor{author in head/foot}{bg=myred}
\setbeamercolor{date in head/foot}{fg=white}

% Highlight elements in some sort of grey.
\setbeamercolor{structure}{fg=mygrey}
\setbeamercolor{normal text}{bg=black, fg=mygrey}

% Use round bullets in lists.
\setbeamertemplate{items}[circle]

% Use bigger fonts for titles.
\setbeamerfont{title}{size=\Huge}
\setbeamerfont{frametitle}{size=\large}

% Don't clutter the pages with useless navigations.
\setbeamertemplate{navigation symbols}{}

\lstdefinestyle{console}{%
	language=bash,
	basicstyle=\ttfamily\fontsize{3.5}{6}\selectfont\color{white},
	frame=none,
	rulecolor=\color{white},
	backgroundcolor=\color{black},
	breaklines=false,
	breakindent=0pt,
	lineskip={0pt},
	escapeinside={<@}{@>}
}

% Author information.
\author[Michael Tremer]{Michael Tremer}
\institute{IPFire Project}

% The title of the presentation.
\title{IPFire 3: Networking}
\subtitle{Basics of the next generation of networking in IPFire}

\date{October 10\textsuperscript{th}, 2015}

\newcommand{\spacer}{\vspace{4 mm}}

\newcommand{\screenshot}[1]{\centerline{%
    \includegraphics[width=\textwidth]{#1}}}

\newcommand{\slug}[1]{
	\vspace*{\fill}

	\begin{center}
		\LARGE #1
	\end{center}

	\vspace*{\fill}
}

\newcommand{\subslug}[1]{
	\slug{\normalsize #1}
}

\begin{document}
	\maketitle

	%\section*{Outline}
	%\frame{\tableofcontents}

	\section{Introduction}

	\begin{frame}
		\frametitle{What is this all about?}

		\slug{The new \texttt{network} command}
		\pause

		\subslug{One to rule them all}
	\end{frame}

	\begin{frame}
		\frametitle{Introduction}

		\slug{Rewritten from scratch}
		\pause

		\subslug{Shell code (because it is fast and easily extensible)}
		\pause

		\subslug{Built around new concepts without any patchwork}
	\end{frame}

	\begin{frame}
		\frametitle{We can do fancy stuff...}

		\subslug{Initialises everything in parallel for fast bootup}
		\subslug{Everything is hotpluggable}
		\subslug{Auto-completion on command line}
		\subslug{Easily extensible = \\
			common language + modular design + huge library of functions for everything}
		\subslug{Good Alpha state, testers welcome}
	\end{frame}

	\section{Zones}

	\begin{frame}
		\slug{\only<2>{Unlimited} Zones}
	\end{frame}

	\begin{frame}
		\frametitle{Zones}

		\slug{Zones represent a \emph{logical} segment of the network}
	\end{frame}

	\subsection{Types of Zones}

	\begin{frame}
		\frametitle{There are two types...}
		\slug{Uplink Zones (\texttt{upl0, upl1, ...})}
		\pause

		\slug{Network Zones (\texttt{net0, net1, ...})}
	\end{frame}

	\subsection{Zone Hooks}

	\begin{frame}
		\frametitle{Zone Hooks}

		\subslug{Every supported protocol is realised as a 'hook'}
		\pause

		\subslug{Hooks are designed as a modular thing}
		\pause

		\subslug{Their only requirement is to create an Ethernet-like
			device \\ that can have an IP address assigned}
	\end{frame}

	\subsubsection{The 'bridge' hook}

	\begin{frame}[fragile]
		\frametitle{The 'bridge' hook}

		\begin{columns}[T] % align columns
			\begin{column}{.48\textwidth}
				\begin{itemize}
					\item The most important hook (i.e. everything local is a bridge)
					\pause
					\item Can have multiple ports
					\begin{itemize}
						\item Packets are forwarded between ports
						\item Redundancy!
						\item Performance!
					\end{itemize}
					\pause
					\item Entire network segments can be added at runtime
					\pause
					\item (True) Layer 2 VPNs
					\pause
				\end{itemize}
			\end{column}%
			\hfill%
			\begin{column}{.48\textwidth}
				\begin{lstlisting}[style=console]
Zone upl0 (enabled, bridge)
  Status                           <@\textcolor{green}{UP}@>
  MTU                              1500

  Statistics
    Received     1946547 packets   170M (0 errors)
    Sent          891860 packets   354M (0 errors)

  Spanning Tree Protocol information
    Version                        Spanning Tree Protocol
    ID                             0200.8e0aa9032f5f
    Priority                       512
    This bridge is root.

    Topology changing              no
    Topology change time           0s
    Topology change count          0

  Ports
    p1            <@\textcolor{green}{FORWARDING}@>  - DSR: 8e:0a:a9:03:2f:5f - Cost: 4
    p2            <@\textcolor{green}{FORWARDING}@>  - DSR: 8e:0a:a9:03:2f:5e - Cost: 4

  Configurations
    ipv4-dhcp          <@\textcolor{green}{UP}@>
      IPv4 address                 192.168.160.126/24
      Gateway                      192.168.160.253

      DNS-Servers                  192.168.160.253
				\end{lstlisting}
			\end{column}%
		\end{columns}
	\end{frame}

	\subsubsection{The 'pppoe' hook}

	\begin{frame}[fragile]
		\frametitle{The 'pppoe' hook}

		\subslug{The PPPoE protocol is used for many dial-up connections
			like DSL, VDSL, some aerial connections or satellite links}
		\subslug{Uses a port to connect to a physical network}
		\subslug{Modular design - Takes a single (physical) port}
	\end{frame}

	\subsubsection{The 'modem' hook}

	\begin{frame}{The 'modem' hook}
		\subslug{Used from 56k modems over UMTS/3G to LTE/4G}
		\subslug{Simply creates a PPP session over a (serial) link}
	\end{frame}

	\begin{frame}{The 'wireless' hook}
		\subslug{Connects to an (encrypted) wireless network}
	\end{frame}

	\begin{frame}
		\frametitle{Other hooks (only IPv6 transition protocols)}

		\begin{center}
			\begin{tabular}{|l|l|}
				\hline
				6to4-tunnel & Hurricane Electric tunnels \\
				\hline
				6rd & The 6rd transitioning protocol used by some ISPs \\
				\hline
				aiccu & AYIYA (anything-in-anything) protocol used by SIXXS.net \\
				\hline
			\end{tabular}
		\end{center}
	\end{frame}

	\begin{frame}
		\frametitle{What have we seen?}

		\subslug{We support everything that is supported in IPFire 2: \\
			Ethernet (Cable + Fibre), WiFi \& PPP (PPPoE, mobile connections)
		}

		\subslug{Except: PPTP}
	\end{frame}

	\section{Ports}

	\subsection{What is a Port?}

	\begin{frame}
		\frametitle{Ports}

		\slug{Ports most often represent a \emph{physical} segment of the network}
		\pause

		\slug{They can also be virtual}
	\end{frame}

	\subsection{Port Hooks}

	\subsubsection{The 'ethernet' hook}

	\begin{frame}[fragile]
		\frametitle{The 'ethernet' hook}

		\begin{columns}[T] % align columns
			\begin{column}{.48\textwidth}
				\subslug{For Ethernet adapters in the system \\
					(physical and virtual)}

				\begin{itemize}
					\item Get automatically created when a new network device is
						plugged in
				\end{itemize}
			\end{column}%
			\hfill%
			\begin{column}{.48\textwidth}
				\begin{lstlisting}[style=console]
[root@prime ~]# network port p1 status
Port p1 (ethernet)
  Status                           <@\textcolor{green}{UP}@>
  Address                          00:c0:08:87:ef:4a
  MTU                              1500
  Promisc                          true

  Statistics
    Received     1952204 packets   197M (0 errors)
    Sent         2461555 packets   432M (0 errors)

[root@prime ~]# network device p1 status
Device status: p1
  Name                             p1
  Status                           <@\textcolor{green}{UP}@>
  Type                             ethernet
  Ethernet-compatible              true
  Address                          00:c0:08:87:ef:4a

  Link                             1000 MBit/s full duplex
  MTU                              1500

  Statistics
    Received     1952262 packets   197M (0 errors)
    Sent         2461604 packets   432M (0 errors)

  Has carrier?                     true
  Promisc                          true
				\end{lstlisting}
			\end{column}%
		\end{columns}
	\end{frame}

	\subsubsection{The 'vlan' hook}

	\begin{frame}[fragile]
		\frametitle{The 'vlan' hook}

		\begin{columns}[T] % align columns
			\begin{column}{.48\textwidth}
				\subslug{Plain 802.1q}
			\end{column}%
			\hfill%
			\begin{column}{.48\textwidth}
				\begin{lstlisting}[style=console]
[root@prime ~]# network port new vlan --parent-device=d0 --tag=10
Configuration check succeeded.
Writing settings file /etc/network/ports/d0v10.
[root@prime ~]# network port d0v10 create
vlan device 'd0v10' has been created
[root@prime ~]# network port d0v10 up
Setting up device 'd0v10'
[root@prime ~]# network port d0v10 status
Port d0v10 (vlan)
  Status                           <@\textcolor{green}{UP}@>
  Address                          b6:2a:2d:f2:ce:13
  MTU                              1500
  Promisc                          false

  Statistics
    Received           0 packets     0B (0 errors)
    Sent               0 packets     0B (0 errors)

  VLAN
    Parent                         d0
    VID                            10
				\end{lstlisting}
			\end{column}%
		\end{columns}
	\end{frame}

	\subsubsection{The 'bonding' hook}

	\begin{frame}[fragile]
		\frametitle{The 'bonding' hook}

		\begin{columns}[T] % align columns
			\begin{column}{.48\textwidth}
				\subslug{Combines several ports to one}

				\begin{itemize}
					\item Also called ``Trunk'', ``LAG'' or ``Bond''
					\item Adds layer 2 failover \& redundancy
					\item Can add the throughput of multiple links
						(20G, 40G, ...)
				\end{itemize}
			\end{column}%
			\hfill%
			\begin{column}{.48\textwidth}
				\begin{lstlisting}[style=console]
[root@prime ~]# network port new bonding b0 --slave=d0 --slave=d1 --mode=802.3ad                                                                                                                         
Configuration check succeeded.
Writing settings file /etc/network/ports/b0.
[root@prime ~]# network port b0 create
Loading module 'bonding'.
Successfully created bonding device 'b0'
Set mode of bond 'b0' to '802.3ad'
Setting address of 'b0' from 'ee:2f:65:02:2e:40' to '9e:db:22:40:46:b0'
[root@prime ~]# network port b0 up
Setting up device 'b0'
[root@prime ~]# network port b0 status
Port b0 (bonding)
  Status                           <@\textcolor{yellow}{NO-CARRIER}@>
  Address                          9e:db:22:40:46:b0
  MTU                              1500
  Promisc                          false

  Statistics
    Received           0 packets     0B (0 errors)
    Sent               0 packets     0B (0 errors)

  Bonding information
    Mode                           802.3ad
    LACP rate                      slow
				\end{lstlisting}
			\end{column}%
		\end{columns}
	\end{frame}

	\subsubsection{The 'wireless-ap' hook}

	\begin{frame}[fragile]
		\frametitle{The 'wireless-ap' hook}

		\begin{columns}[T] % align columns
			\begin{column}{.48\textwidth}
				\begin{itemize}
					\item For Ethernet adapters in the system \\
						(physical and virtual)
				\end{itemize}
			\end{column}%
			\hfill%
			\begin{column}{.48\textwidth}
				\begin{lstlisting}[style=console]
[root@prime ~]# network port new bonding b0 --slave=d0 --slave=d1 --mode=802.3ad                                                                                                                         
Configuration check succeeded.
Writing settings file /etc/network/ports/b0.
[root@prime ~]# network port b0 status
Port b0 (unknown)
  Status                           <@\textcolor{red}{DOWN}@>
  Address                          
[root@prime ~]# network port b0 create
Loading module 'bonding'.
Successfully created bonding device 'b0'
Set mode of bond 'b0' to '802.3ad'
Setting address of 'b0' from 'ee:2f:65:02:2e:40' to '9e:db:22:40:46:b0'
[root@prime ~]# network port b0 up
Setting up device 'b0'
[root@prime ~]# network port b0 status
Port b0 (bonding)
  Status                           <@\textcolor{yellow}{NO-CARRIER}@>
  Address                          9e:db:22:40:46:b0
  MTU                              1500
  Promisc                          false

  Statistics
    Received           0 packets     0B (0 errors)
    Sent               0 packets     0B (0 errors)

  Bonding information
    Mode                           802.3ad
    LACP rate                      slow
				\end{lstlisting}
			\end{column}%
		\end{columns}
	\end{frame}

	\begin{frame}
		\frametitle{Other hooks}

		\begin{center}
			\begin{tabular}{|l|l|}
				\hline
				wireless-adhoc & Wireless Ad-hoc networks \\
				\hline
				dummy & Dummy devices \\
				\hline
				batman-adv & For wireless mesh networks with B.A.T.M.A.N \\
				\hline
			\end{tabular}
		\end{center}
	\end{frame}

	\section{Zones \& Ports}
	\subsection{Putting it all together}

	\begin{frame}[fragile]
		\frametitle{Putting it all together}

		\begin{columns}[T] % align columns
			\begin{column}{.48\textwidth}
				\subslug{Ports get attached to \\ and detached from zones}
				\pause

				\begin{lstlisting}[style=console]
[root@prime ~]# network zone new net9 bridge
<@\textcolor{blue}{Configuration check succeeded.}@>
<@\textcolor{blue}{Writing settings file /etc/network/zones/net9/settings.}@>
Auto-start enabled for zone net9
Started service 'network@net9.service', code=0.
[root@prime ~]# network zone net9 port attach b0
<@\textcolor{blue}{Configuration check succeeded.}@>
<@\textcolor{blue}{Writing settings file /etc/network/zones/net9/ports/b0.}@>
b0 has been attached to net9
bridge: device 'b0' has been attached to bridge 'net9'
[root@prime ~]# network zone net9 port attach d0v10
<@\textcolor{blue}{Configuration check succeeded.}@>
<@\textcolor{blue}{Writing settings file /etc/network/zones/net9/ports/d0v10.}@>
d0v10 has been attached to net9
bridge: device 'd0v10' has been attached to bridge 'net9'
				\end{lstlisting}
				\pause
			\end{column}%
			\hfill%
			\begin{column}{.48\textwidth}
				\begin{lstlisting}[style=console]
[root@prime ~]# network  status net9
Zone net9 (enabled, bridge)
  Status                           <@\textcolor{green}{UP}@>
  MTU                              1500

  Statistics
    Received           0 packets     0B (0 errors)
    Sent               7 packets   738B (0 errors)

  Spanning Tree Protocol information
    Version                        Rapid Spanning Tree Protocol
    ID                             0200.caddf06a9f59
    Priority                       512
    This bridge is root.

    Topology changing              no
    Topology change time           5m
    Topology change count          0

  Ports
    b0            <@\textcolor{yellow}{NO-CARRIER}@>
    d0v10         <@\textcolor{green}{FORWARDING}@>  - DSR: ca:dd:f0:6a:9f:59
                              - Cost: 2000000
				\end{lstlisting}
			\end{column}%
		\end{columns}
	\end{frame}

	\begin{frame}
		\includegraphics[width=\textwidth]{res/zone}
	\end{frame}

	\section{Configurations}

	\subsection{What is a Config?}

	\begin{frame}
		\frametitle{Configs}

		\slug{Everything has been only layer 2 so far... \\
			Configs add layer 3}
	\end{frame}

	\subsection{Config Hooks}

	\subsubsection{IPv6 \& IPv4 static configuration}

	\begin{frame}[fragile]
		\frametitle{The 'ipv\{6,4\}-static' hook}

		\begin{columns}[T] % align columns
			\begin{column}{.48\textwidth}
				\subslug{Static IP address assignment \\ for IPv6 and IPv4}
			\end{column}%
			\hfill%
			\begin{column}{.48\textwidth}
				\begin{lstlisting}[style=console]
[root@prime ~]# network zone net9 config new ipv6-static \
           --address=2001:db8:: --prefix=32                                                                                                                          
Configuration check succeeded.
Writing settings file /etc/network/zones/net9/configs/ipv6-static.2001db8.32.
IP address '2001:db8::' (ipv6) was successfully configured on device 'net9'.
[root@prime ~]# ip addr show dev net9
22: net9@NONE: <BROADCAST,MULTICAST,PROMISC,UP,LOWER_UP> mtu 1500 qdisc noqueue state UP group default 
    link/ether ca:dd:f0:6a:9f:59 brd ff:ff:ff:ff:ff:ff
    inet6 2001:db8::/32 scope global 
       valid_lft forever preferred_lft forever
    inet6 fe80::c8dd:f0ff:fe6a:9f59/64 scope link 
       valid_lft forever preferred_lft forever
				\end{lstlisting}
			\end{column}%
		\end{columns}
	\end{frame}

	\begin{frame}[fragile]
		\frametitle{The 'ipv\{6,4\}-dhcp' hook}

		\begin{columns}[T] % align columns
			\begin{column}{.48\textwidth}
				\subslug{Dynamic Host Configuration Protocol}
			\end{column}%
			\hfill%
			\begin{column}{.48\textwidth}
				\begin{lstlisting}[style=console]
[root@prime ~]# network zone net9 config new ipv4-dhcp
Started service 'dhclient4@net9.service', code=0.
				\end{lstlisting}
			\end{column}%
		\end{columns}
	\end{frame}

	\begin{frame}
		\frametitle{Other hooks}

		\begin{center}
			\begin{tabular}{|l|l|}
				\hline
				pppoe-server & Runs a PPPoE server on a zone with IPv6 \& IPv4 \\
				\hline
				ipv6-auto & IPv6 auto-configuration (which is pretty much useless for us) \\
				\hline
			\end{tabular}
		\end{center}
	\end{frame}

	\section{More}

	\begin{frame}
		\slug{Static Routes}
	\end{frame}

	\begin{frame}
		\slug{DNS}

		\subslug{Imports DNS servers from dynamically configured connections}
	\end{frame}

	\begin{frame}
		\slug{DHCP Server}
	\end{frame}

	\section{The End}

	\begin{frame}
		\begin{center}
			\includegraphics[height=.5\textheight]{res/ipfire_tux_512x512.png}
		\end{center}

		\begin{itemize}
			\item Code: \url{git://git.ipfire.org/network.git}
			\item GitWeb: \url{http://git.ipfire.org/?p=network.git;a=summary}
		\end{itemize}
	\end{frame}
\end{document}
