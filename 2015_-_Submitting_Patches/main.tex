
\documentclass[serif,mathserif,compress]{beamer}

\usepackage{beamerthemesplit}

\usetheme{default}
\useoutertheme{default}

\usepackage[british]{babel}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{color}
\usepackage{epsfig}
\usepackage{listings}
\usepackage{marvosym}
\usepackage{texnansi}
\usepackage{verbatim}
\usepackage{xspace}

% Make this a 16:9 presentation
\setlength{\paperwidth}{171 mm}
\setlength{\paperheight}{96 mm}
\setlength{\textwidth}{151 mm}
\setlength{\textheight}{86 mm}

\usepackage[default,osfigures,scale=0.95]{opensans}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}

% Set sans-serif font.
%\renewcommand\sfdefault{phv}
%\renewcommand\familydefault{\sfdefault}

% Define some colours.
\definecolor{myred}{rgb}{0.53,0.01,0}
\definecolor{mygrey}{rgb}{0.3,0.3,0.3}

% Make a nice gradient as background.
%\setbeamertemplate{background canvas}[vertical shading]
%[bottom=black, middle=myred, top=myred]
\setbeamercolor{background canvas}{bg=white, fg=mygrey}
\setbeamercolor{frametitle}{fg=mygrey,bg=mygrey!20}
\setbeamercolor{section in head/foot}{bg=myred}
\setbeamercolor{subsection in head/foot}{fg=white}
\setbeamercolor{author in head/foot}{bg=myred}
\setbeamercolor{date in head/foot}{fg=white}

% Highlight elements in some sort of grey.
\setbeamercolor{structure}{fg=mygrey}
\setbeamercolor{normal text}{bg=black, fg=mygrey}

% Use round bullets in lists.
\setbeamertemplate{items}[circle]

% Use bigger fonts for titles.
\setbeamerfont{title}{size=\Huge}
\setbeamerfont{frametitle}{size=\large}

% Don't clutter the pages with useless navigations.
\setbeamertemplate{navigation symbols}{}

\lstdefinestyle{console}{%
	language=bash,
	basicstyle=\ttfamily\fontsize{3.5}{6}\selectfont\color{white},
	frame=none,
	rulecolor=\color{white},
	backgroundcolor=\color{black},
	breaklines=false,
	breakindent=0pt,
	lineskip={0pt},
	escapeinside={<@}{@>}
}

% Author information.
\author[Michael Tremer]{Michael Tremer}
\institute{IPFire Project}

% The title of the presentation.
\title{Submitting Patches}
\subtitle{Contributing to the IPFire project made easy}

\date{October 10\textsuperscript{th}, 2015}

\newcommand{\spacer}{\vspace{4 mm}}

\newcommand{\screenshot}[1]{\centerline{%
    \includegraphics[width=\textwidth]{#1}}}

\newcommand{\slug}[1]{
	\vspace*{\fill}

	\begin{center}
		\LARGE #1
	\end{center}

	\vspace*{\fill}
}

\newcommand{\subslug}[1]{
	\slug{\normalsize #1}
}

\begin{document}
	\maketitle

	%\section*{Outline}
	%\frame{\tableofcontents}

	\section{Introduction}

	\begin{frame}
		\slug{We are an Open Source project \\
			\pause ... and use an open development approach}
	\end{frame}

	\begin{frame}
		\slug{Everyone is welcome to contribute}
		\subslug{No skills required}
	\end{frame}

	\begin{frame}
		\slug{This might look complicated \\
			- it is mostly common sense}
	\end{frame}

	\begin{frame}
		\slug{Why does all this matter? \\
			\pause Our brains are good with patterns}
	\end{frame}

	\section{Obtaining the Sources}

	\begin{frame}
		\slug{We are using \texttt{git}}
	\end{frame}

	\begin{frame}
		\slug{Everything we do is in a git repository somewhere on \\
			\url{http://git.ipfire.org}}
	\end{frame}

	\subsection{Setting up Git}

	\begin{frame}
		\frametitle{Setting up Git}

		\slug{Introduce yourself}

		\subslug{We want to know who you are \\
			Real name required}
		\pause

		\subslug{
			\texttt{git config ----global user.name ``Michael Tremer''} \\
			\texttt{git config ----global user.email michael.tremer@ipfire.org}
		}
	\end{frame}

	\subsection{Setting up Git for Email}

	\begin{frame}
		\frametitle{Setting up Git for Email}

		\slug{Git needs to send email...}

		\subslug{
			\texttt{git config ----global sendemail.smtpencryption tls} \\
			\texttt{git config ----global sendemail.smtpserver mail01.ipfire.org} \\
			\texttt{git config ----global sendemail.smtpserverport 587} \\
			\texttt{git config ----global sendemail.smtpuser ms}
		}

		\subslug{(You can use what ever mail server you want...)}
	\end{frame}

	\subsection{Creating a Development Branch}

	\begin{frame}
		\frametitle{Creating a Development Branch}

		\begin{columns}[T] % align columns
			\begin{column}{.78\textwidth}
				\slug{We use development branches for each feature}

				\subslug{
					\texttt{git checkout -b my-feature -t origin/master}
				}
			\end{column}%
			\hfill%
			\begin{column}{.28\textwidth}
				\includegraphics[height=.85\textheight]{res/git-branches}
			\end{column}%
		\end{columns}
	\end{frame}

	\section{Making Changes}

	\begin{frame}
		\slug{Time to hack...}
		\pause

		\subslug{But there are some rules...}
	\end{frame}

	\begin{frame}
		\subslug{Check with us before starting working on bigger things}
		\pause

		\subslug{Separate your changes \\
			e.g. bug fixes vs. performance enhancements}
		\pause

		\subslug{Send small patches \\ each one justifiable on its own merits}
		\pause
		
		\subslug{Make sure that everything builds \\ mark dependencies}
		\pause

		\subslug{Style-check your changes}
		\pause

		\subslug{Test!}
	\end{frame}

	\subsection{Committing Changes}

	\begin{frame}
		\frametitle{Committing Changes}

		\slug{Committing a patch means \\ adding it to the project's history}
		\pause

		\subslug{It will be in there for forever}
	\end{frame}

	\subsection{Writing a commit message}

	\begin{frame}
		\frametitle{Writing a good commit message}

		\slug{Describe your changes}
		\pause

		\subslug{What motivated you to develop this patch?}
		\subslug{What is the impact of the patch?}
		\subslug{Are there user-visible changes?}
	\end{frame}

	\begin{frame}
		\frametitle{Writing a good commit message}

		\slug{Describe \underline{how} you solved the problem}
		\pause

		\subslug{Include technical details}
		\subslug{(If this step is getting difficult your patch is probably too complex)}
	\end{frame}

	\begin{frame}
		\frametitle{Writing a good commit message}

		\slug{Add useful information}
		\pause

		\subslug{Bug IDs}
		\subslug{Mailing list discussions \\
			(including a quick summary)}
	\end{frame}

	\subsection{Git Tags}

	\begin{frame}
		\frametitle{Git tags - Who did what?}

		\slug{Sign your work}

		\subslug{\texttt{Signed-off-by: Michael Tremer <michael.tremer@ipfire.org>}}
	\end{frame}

	\begin{frame}
		\frametitle{Git tags - Who did what?}

		\slug{Credit your collaborators}

		\subslug{Reported-by: | Tested-by: | Reviewed-by: | Suggested-by:}
	\end{frame}

	\subsection{Why all this?}

	\begin{frame}
		\slug{Why all this?}
		\pause

		\subslug{Commit messages will be read by the reviewers of your patch... \\
			\pause (help them to make reviewing your changes easier)}
			
		\pause
		\subslug{...and after many years}
	\end{frame}

	\section{Review}

	\begin{frame}
		\slug{Every patch must be reviewed}

		\subslug{Respond to any comments - don't get discouraged - or impatient}
	\end{frame}

	\begin{frame}
		\frametitle{Who to send patches to?}

		\slug{The maintainer of a subsystem}
		\slug{\underline{and} at least one mailing list}
	\end{frame}

	\subsection{Submitting Patches}

	\begin{frame}
		\slug{We use email only}

		\subslug{Pull requests only for bigger changes \& maintainers \\
			No GitHub pull requests}
	\end{frame}

	\begin{frame}
		\frametitle{Patch format}

		\begin{itemize}
			\item Must include a From: header (with the real name and email address)
			\item Subject line starting with \texttt{[PATCH]}
			\item The commit message
			\item \texttt{Signed-off-by:} line
			\item The actual changes in the unified diff format
		\end{itemize}
		\pause

		\slug{\texttt{git send-email} automates all that}
	\end{frame}

	\subsection{Patchwork}

	\subsection{Track what is going on...}

	\begin{frame}
		\frametitle{Track what is going on...}

		\slug{\url{http://patchwork.ipfire.org/}}
		\pause

		\subslug{Help us reviewing patches}
	\end{frame}

	\section*{The End}

	\begin{frame}
		\begin{center}
			\includegraphics[height=.5\textheight]{res/ipfire_tux_512x512.png}
		\end{center}

		\subslug{
			Read everything on \\
			\url{http://wiki.ipfire.org/devel/submit-patches}
		}
	\end{frame}
\end{document}
