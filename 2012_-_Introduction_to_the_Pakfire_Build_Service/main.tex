
\documentclass[serif,mathserif]{beamer}

\usepackage{beamerthemesplit}

\usetheme{default}
\useoutertheme{default}

\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{color}
\usepackage{epsfig}
\usepackage{marvosym}
\usepackage{texnansi}
\usepackage{verbatim}
\usepackage{xspace}

% Make this a 16:9 presentation
%\setlength{\paperwidth}{171 mm}
%\setlength{\paperheight}{96 mm}
%\setlength{\textwidth}{151 mm}
%\setlength{\textheight}{86 mm}

% Set sans-serif font.
\renewcommand\sfdefault{phv}
\renewcommand\familydefault{\sfdefault}

% Define some colours.
\definecolor{myred}{rgb}{0.53,0.01,0}
\definecolor{mygrey}{rgb}{0.4,0.4,0.4}

% Make a nice gradient as background.
\setbeamertemplate{background canvas}[vertical shading]
[bottom=black, middle=myred, top=myred]

% Highlight elements in some sort of grey.
\setbeamercolor{structure}{fg=mygrey}
\setbeamercolor{normal text}{bg=black, fg=white}

% Use round bullets in lists.
\setbeamertemplate{items}[circle]

% Use bigger fonts for titles.
\setbeamerfont{title}{size=\Huge}
\setbeamerfont{frametitle}{size=\large}
\setbeamertemplate{frametitle}[default]

% Don't clutter the pages with useless navigations.
\setbeamertemplate{navigation symbols}{}

% Author information.
\author[Michael Tremer]{Michael Tremer}
\institute{IPFire Project}

% The title of the presentation.
\title{Introduction to the\\ Pakfire Build Service}
\subtitle{The new IPFire build system}

\date{April 28, 2012}

\newcommand{\spacer}{\vspace{4 mm}}

\newcommand{\screenshot}[1]{\centerline{%
    \includegraphics[width=\textwidth]{#1}}}

\begin{document}
	\maketitle

	%\section*{Outline}
	%\frame{\tableofcontents}

	\section{Introduction}
	\subsection{What is the Pakfire Build Service?}
	\frame{
		\begin{center}
			The Pakfire Build Service is the central build system
			of the IPFire firewall distribution.
		\end{center}
	}
	\frame{
		\begin{center}
			It automates the whole process from source code to a binary package
			that can be easily installed on any IPFire system.
		\end{center}
	}

	\subsection{First things first\dots Terminology}
	\frame{
		\frametitle{What is a package?}

		In general, a package is a collection of files and their metadata.
		\spacer \pause

		It is a single file and hence can easily be sent around for further
			development or distribution. File Extension: pfm.
		\spacer \pause

		There are two different types of packages\dots
		\pause

		\begin{itemize}
			\item<4-> Source packages
			\item<5-> Binary packages
		\end{itemize}
	}

	\subsubsection{Source packages}
	\frame{
		\frametitle{Source packages}

		\begin{itemize}
			\item<2-> A source package contains source code and
				build instructions to compile the sources.

			\item<3-> Source code is considered to be the release tarball
				of an upstream project and a number of patches.

			\item<4-> The result after putting a source package into a build
				is one or more binary packages.
		\end{itemize}
	}
	%\frame{
	%	\frametitle{Source packages: Makefiles}
	%
	%	The heart of a source package is the \emph{Makefile} which is always
	%	called \emph{package-name.nm}.
	%	\spacer
	%}

	\subsubsection{Binary packages}
	\frame{
		\frametitle{Binary packages}

		Binary packages are ready for installation on target systems.
		\spacer

		It's easy:
		\begin{center}
			\emph{pakfire install htop-1.0.1-1.ip3.x86\_64.pfm}
		\end{center}
	}
	\frame{
		\frametitle{Binary packages: Example}

		\emph{audit - User space tools for 2.6 kernel auditing}

		\begin{itemize}
			\item audit-2.2-2.ip3.src.pfm
		\end{itemize}
		\spacer \pause

		The source package produces a whole set of binary packages:
		\begin{itemize}
			\item audit-2.2-2.ip3.x86\_64.pfm
			\item audit-debuginfo-2.2-2.ip3.x86\_64.pfm
			\item audit-devel-2.2-2.ip3.x86\_64.pfm
			\item audit-libs-2.2-2.ip3.ip3.x86\_64.pfm
		\end{itemize}
	}

	\subsubsection{Packages have got dependencies}
	\frame{
		\frametitle{Packages have got dependencies}

		Packages have different kinds of \emph{dependencies}
		which define relationships to each other.
		\spacer \pause

		A dependency can be defined as:
		\spacer

		\begin{itemize}
			\item<3-> An other package by name (\emph{openssl, beep or zlib-devel}).
			\item<4-> An other package by name and version (\emph{glibc >= 2.15 or udev < 182}).
			\item<5-> A filename like \emph{/usr/bin/bash}.
			\item<6-> A string that is provided by any package (e.g. \emph{bootloader}).
				You should not use this.
		\end{itemize}
	}
	\frame{
		\frametitle{Packages have got dependencies}

		A given package \emph{X} may have the following dependencies:
		\spacer \pause

		\begin{itemize}
			\item<2-> \emph{Requires}: The installation of package \emph{X}
				requires the availability of everything in this list.
			\item<3-> \emph{Provides}: The package in hand comes with the
				features in this list.
			\item<4-> \emph{Conflicts}: Package X cannot be installed if
				any of these dependencies is provided by an other package.
			\item<5-> \emph{Obsoletes}: If package X is installed, there is
				no need for anything in this list.
		\end{itemize}
	}
	\frame{
		\frametitle{Benefits of the package division}

		For users:
		\begin{itemize}
			\item<2-> Install only what you need.
			\begin{itemize}
				\item Keeps the system small and fast.
			\end{itemize}

			\item<3-> Better updates:
			\begin{itemize}
				\item Update only what you want (e.g. only security updates).
				\item Faster distribution of hotfixes.
				\item Rollback bad updates.
			\end{itemize}

			\item<4-> Use other package sources.
			\begin{itemize}
				\item Third party repositories for closed source stuff.
				\item Packages we don't want to have in IPFire.
				\item Whatever you might think of...
			\end{itemize}
		\end{itemize}
	}
	\frame{
		\frametitle{Benefits of the package division}

		For developers:
		\begin{itemize}
			\item<2-> Each developer gets a small chunk to maintain.
			\item<3-> One broken package does not break the whole distribution.
			\item<4-> Faster builds:
			\begin{itemize}
				\item Just rebuild one package.
				\item Never the whole distribution.
			\end{itemize}

			\item<5-> Send unstable updates to each other. Test them.

			\item<6-> Push out updates earlier:
			\begin{itemize}
				\item No release planning for core updates.
				\item Don't bother with issues that have already been fixed.
			\end{itemize}
		\end{itemize}
	}

	\subsection{Conclusion}
	\frame{
		\frametitle{Conclusion}

		Packages have a lot of advantages.
		\spacer \pause

		But the management of more than 1250 packages per architecture
		is a tough job.
		\spacer \pause

		We need a management tool for that!
		\spacer
	}

	\section{The Pakfire Build Service}
	\frame{
		\frametitle{The Pakfire Build Service (PBS)}

		\only<1>{
			\begin{figure}
				\screenshot{res/pbs-homepage.png}
			\end{figure}
		}

		\begin{itemize}
			\item<2-> It has been developed by developers of the IPFire project to
				achieve a very high level of quality in the build process in
				conjunction with the community.
			\item<3-> Written in pure Python.
			\item<4-> Comes with a web user interface and an XMLRPC management
				interface.
			\item<5-> Heavily based on the Pakfire package management system.
		\end{itemize}
	}

	\subsection{Excursion: pakfire}
	\frame{
		\frametitle{Excursion: pakfire}

		The Pakfire package management system manages installations, updates,
		removes and more of packages on an IPFire system.
		\spacer

		\begin{itemize}
			\item<2-> Written in Python with performance critical code in C.
			\item<3-> Fast and most robust dependency solving algorithm in the world.
			\item<4-> Very tiny. Runs on embedded systems.
			\item<5-> Built-in build system.
		\end{itemize}
		\spacer
	}

	\subsection{More definitions}
	\frame{
		\frametitle{More definitions}

		\begin{block}{Package}
			Primarily, the term \emph{package} describes a source package.
			It also can rarely mean the actual package file.
		\end{block}
		\pause

		\begin{block}{Build}
			A build is an ordering unit which contains a source package
			and multiple \emph{build jobs}. There is also some meta information
			about the reason of the build (commit message, associated bugs).\par
			A build is what we are managing here. We don't bother about
			a single package file.
		\end{block}
		\pause

		\begin{block}{Build job or just: job}
			A build contains several jobs. One for each architecture the
			build is built for.
		\end{block}
		\pause
	}

	\subsection{A package}
	\frame{
		\frametitle{PBS by example: A package}

		\begin{columns}[c]
			\column{.5\textwidth}
			\begin{figure}
				\screenshot{res/pbs-package-kernel.png}
			\end{figure}

			\column{.5\textwidth}
			This is the kernel package.
			\spacer

			The page contains some general information about the package
			and in the row below a list of all known builds.
			\spacer

			At the bottom: Open bugs, some statistics.
		\end{columns}
	}

	\subsection{A build}
	\frame{
		\frametitle{PBS by example: A build}

		A build can be created in two ways and thus they
		distinguish themselves in two categories.
		\spacer

		\begin{itemize}
			\item<2-> Release builds.
			\item<3-> Scratch builds.
		\end{itemize}
	}

	\subsubsection{A release build}
	\frame{
		\frametitle{A release build}

		A release build is created out of a commit in
		a git repository.
		\spacer

		\begin{itemize}
			\item<2-> It is automatically created from source.
			\item<3-> Always linked to a distribution.
			\item<4-> Goes its way through all repositories of the distribution
				(i.e. from testing to stable).
		\end{itemize}
	}

	\subsubsection{A scratch build}
	\frame{
		\frametitle{A scratch build}

		In contrast to the release builds, a scratch build
		is created by an individual.
		\spacer

		\begin{center}
			Command: \emph{ pakfire-client build beep/beep.nm}
		\end{center}

		\begin{itemize}
			\item<2-> Used to test if a packages does build on all architectures.
			\item<3-> Used to distribute experimental packages.
		\end{itemize}
		\spacer \pause \pause \pause

		We will learn what exciting things we can
		do with them in a minute\dots
	}

	\section{Learning by example}

	\subsection{Step by step}
	\frame{
		\frametitle{Step by step}

		\begin{center}
			Going step by step through the web user interface of the Pakfire
			Build Service, you will get an impression about what it can do...
		\end{center}
	}

	\subsection{A package}
	\frame{
		\frametitle{A package}

		\begin{figure}
			\screenshot{res/pbs-package-gcc.png}
		\end{figure}
	}
	\frame{
		\frametitle{A package}

		\begin{figure}
			\screenshot{res/pbs-package-gcc-top.png}
		\end{figure}

		At the top of the page, you will find the most important metadata of
		the package \pause which is:\par name and a short sentence what the package
		does (headline)\pause, a longer description\pause, the home of the
		package (we call this upstream)\pause, license\pause,
		and the maintainer within the IPFire project.
	}
	\frame{
		\frametitle{A package}

		\begin{figure}
			\screenshot{res/pbs-package-gcc-btm.png}
		\end{figure}

		At the bottom you will find a list of all builds for this package.
		\spacer
		
		That are two release builds for this example, one in testing,
		the other already in stable state.
		\spacer

		Let's have a closer look at them...
	}

	\subsection{A build}
	\frame{
		\frametitle{A build}

		\begin{figure}
			\screenshot{res/pbs-build-gcc.png}
		\end{figure}
	}
	\frame{
		\frametitle{A build}

		\begin{figure}
			\screenshot{res/pbs-build-gcc-top.png}
		\end{figure}

		The same metadata as we already know from the package page.
		\spacer

		Additionally tagged as a release build or scratch build in the top
		right corner.
	}
	\frame{
		\frametitle{A build}

		Further down, there is a block that shows us more information about
		the reason for this build and the current state.
		\spacer

		\begin{figure}
			\screenshot{res/pbs-build-gcc-mdl.png}
		\end{figure}
		\pause

		This build for example was created by commit \emph{c6952ba} and
		is in the testing repository.
	}
	\frame{
		\frametitle{A build}

		At the bottom of a package is a short summary of the build jobs.

		You get an overview about how the build of the package goes.
		\spacer
		
		\begin{figure}
			\screenshot{res/pbs-build-gcc-jobs.png}
		\end{figure}
	}
	\frame{
		\frametitle{A build}

		By clicking on the \emph{Properties} tab, you can see a couple
		of more meta information about this build.
		\spacer

		\begin{figure}
			\screenshot{res/pbs-build-gcc-properties.png}
		\end{figure}
	}
	\frame{
		\frametitle{A build: Comments \& score}

		One major design goal of the PBS was the involvement of the community
		of testers into the development process.
		\spacer \pause

		We achieved that by giving our users the opportunity to comment on
		builds and rate them like in this example:
		\spacer

		\begin{figure}
			\screenshot{res/pbs-build-gcc-log.png}
		\end{figure}
	}
	\frame{
		We gain the following advantages:

		\begin{itemize}
			\item<2-> Users get go and grab a newer package if they are experiencing
				an issue with the current version.
			\item<3-> If the package fixes their bug, they only need to type a few
				lines into the comment box and rate the build.
			\item<4-> The developer gets immediate feedback about what he has
				created. This has been difficult in the past.
			\item<5-> If things don't go that well, it's the same game.
		\end{itemize}
		\pause \pause \pause \pause \pause

		At the end of the day, it is much easier for the developer to decide if
		a package is broken or if the update is working well and worth being
		\emph{pushed}.
	}
	\frame{
		\begin{columns}[c]
			\column{.5\textwidth}
			Who is allowed to comment and vote?
			\spacer

			Every registered user is allowed to comment builds.
			\spacer

			At the current point in time, it is necessary to be enabled
			by an admin to vote to assure a certain quality.
			\spacer

			\column{.5\textwidth}
			\begin{figure}
				\screenshot{res/pbs-build-kernel-comment.png}
			\end{figure}
		\end{columns}
	}

	\subsection{A job}
	\frame{
		\frametitle{A job}

		\begin{figure}
			\screenshot{res/pbs-job-gcc-top.png}
		\end{figure}

		On a job page, you will find a lot of information about the state
		and progress of a build job for a certain architecture.
		\spacer

		\begin{center}
			\small NOTE: The interface may still change a bit.
		\end{center}
	}
	\frame{
		You have access to the build log files which contain the whole
		build process and are very useful for debugging.
		\spacer

		\begin{figure}
			\screenshot{res/pbs-job-gcc-logfiles.png}
		\end{figure}

		They are easy to download or can be directly viewed in the web browser.
	}
	\frame{
		Another very important feature is the so called \emph{buildroot}.
		\spacer \pause

		If there is a package with a serious bug which affected other packages
		that have been built with that package, we need to know which
		packages are infected.
		\spacer \pause

		The buildroot gives us a list of all packages that were extracted to
		the buildroot at this build job.
		\spacer \pause

		Another application: If a build fails, we are able to create the
		\underline{exactly} same buildroot and reproduce the complete build
		process.
	}
	\frame{
		\begin{figure}
			\screenshot{res/pbs-job-gcc-buildroot.png}
		\end{figure}
	}
	\frame{
		A build job also owns the package files that came out of the build process.

		\begin{figure}
			\screenshot{res/pbs-job-gcc-files.png}
		\end{figure}
	}
	\frame{
		\begin{center}
			Let's click on one of these...
		\end{center}
	}

	\subsection{A package file}
	\frame{
		\frametitle{A package file}

		 Again, we have a lot of meta information which explains itself.

		\begin{figure}
			\screenshot{res/pbs-package-libgcc-top.png}
		\end{figure}
	}
	\frame{
		\begin{columns}[c]
			\column{.5\textwidth}
			You may view the dependencies of a package online\dots

			\column{.5\textwidth}
			\begin{figure}
				\screenshot{res/pbs-package-libgcc-deps.png}
			\end{figure}
		\end{columns}
	}
	\frame{
		\dots as well as the list of files in the package (with permissions
		and user) which is both very useful for debugging.
	
		\begin{figure}
			\screenshot{res/pbs-package-libgcc-files.png}
		\end{figure}
	}

	\section{The end}
	\subsection{Conclusion}
	\frame{
		\frametitle{Conclusion}

		In conclusion, the Pakfire Build Service has increased our productivity
		enormously, because it is so much faster in bringing out package updates.
		\spacer \pause

		Bugs get fixed very fast and do not stop you working on something.
		\spacer \pause

		Developers are able to test each others changes very easily and
		provide feedback at a central place. So are users.
		\spacer \pause

		The integration with the bugtracker (Bugzilla) closes bugs
		and makes sure that the reporter of a bug is informed about a fix
		and encouraged to test it.
		\spacer
	}
	\frame{
		Altogether, we are very proud of what we have done.
		\spacer \pause

		The Pakfire Build Service makes development fun. It helps to achieve
		higher goals.
		\spacer \pause

		We don't consider the service fully done, because we have so many
		ideas on our minds\dots
	}

	\frame{
		\begin{center}
			\Huge{THE END}
			\spacer
		\end{center}
		
		\begin{center}
			Now go out and show what you have learned today at
			\spacer \url{https://pakfire.ipfire.org}
		\end{center}
	}
\end{document}
